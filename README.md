# PHP MVC reto

## Description

Esta aplicacion es un ejemplo de como se puede implementar el patron de diseño MVC
para ello se desarrolla una aplicacion que realiza un CRUD sobre una tabla
siguindo el proceso indicado en la guia titulada "Prueba Técnica MVC PHP"
la aplicacion se a desarrollado utilizando los framework LARVEL y VUEJS esta aplicacion
esta hecha para ser una SPA ( single-page application).


## Installation

Despues de clonar este repositorio se deben installar las depencias utilizando composer
```
composer install
```
Luego de instalar las dependencias, renombre el archivo .env.example a .env la base de datos a utilizar en este archivo
en la seccion DB. Luego de completar este proceso ejecute el siguiente commando para generar la clave que LARAVEL utilizara para encriptar las cookies y otros datos
```
php artisan key:generate
```
A continuacion ejecute el siguiente commando para generar las tablas en la 
base de datos previamente configurada.
```
php artisan migrate
```
Para ejecutar el servidor de desarrollo corra el siguiente commando
```
php artisan serve
```

Para mas documentacion de como puede desplegar una aplicacion desarrollada 
en LARAVEL consulte la documentacion oficial https://laravel.com/docs/5.6.