<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Process extends Model
{
    protected $fillable = ['description', 'sede', 'budget', 'usd_budget'];
    protected $hidden = ['updated_at'];
    protected $casts = [
        'created_at' => 'date:d/m/Y',
    ];

}
