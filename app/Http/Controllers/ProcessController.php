<?php

namespace App\Http\Controllers;

use App\Process;
use Illuminate\Http\Request;

class ProcessController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Process::orderBy('id', 'desc')->paginate(5);
    }

    /**
     * Show the specified resource.
     *
     * @param  \App\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function show(Process $process)
    {
        return $process;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'description' => 'required|max:200',
            'sede' => 'nullable|in:bogota,mexico,peru',
            'budget' => 'nullable|numeric'
        ]);

        // Add usd convert if budget exist
        if(!empty($validatedData['budget'])){
          $budget = $validatedData['budget'];
          $validatedData['usd_budget'] = $this->convertCopToUsd($budget);
        }

        // Save data
        $processSaved = Process::create($validatedData);

        return response()->json([
            'status' => 'ok',
            'data' => $processSaved
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Process $process)
    {
         $validatedData = $request->validate([
            'description' => 'required|max:200',
            'sede' => 'nullable|in:bogota,mexico,peru',
            'budget' => 'nullable|numeric'
        ]);

        // Add usd convert if budget exist
        if(!empty($validatedData['budget'])){
          $budget = $validatedData['budget'];
          $validatedData['usd_budget'] = $this->convertCopToUsd($budget);
        }

        // Update Data
        $process->update($validatedData);

        return response()->json([
            'status' => 'ok',
            'data' => $process
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Process  $process
     * @return \Illuminate\Http\Response
     */
    public function destroy(Process $process)
    {
        // Delete Data
        $processDeleted =$process->delete();

        return response()->json([
            'status' => 'ok',
            'data' => $process
        ]);
    }

    public function search(Request $request)
    {
        $create_date = $request->date;
        $sede = $request->sede;
        $description = $request->description;

        $process = Process::when($create_date, function($query, $create_date){
           return $query->whereDate('created_at', $create_date);
        })
        ->when($sede, function($query, $sede){
          return $query->where('sede', $sede);
        })
        ->when($description, function($query, $description){
          return $query->where('description', 'like' ,'%'.$description.'%');
        })
        ->orderBy('id', 'desc');

        return $process->paginate(5);
    }


    /**
     * Convert cop to usd
     *
     * @param  double  $value
     * @return double
     */
    private function convertCopToUsd($value)
    {
        return sprintf('%0.3f', 0.00035 * $value);
    }
}
