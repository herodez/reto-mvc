
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue';
import VueRouter from 'vue-router';
import VueEvents from 'vue-events'
import VueFlashMessage from 'vue-flash-message';
window.Vue = require('vue');

Vue.use(VueRouter);
Vue.use(VueEvents)
Vue.use(VueFlashMessage);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import NewProcess from './components/NewProcess.vue';
import ViewProcess from './components/ViewProcess.vue';
import EditProcess from './components/EditProcess.vue';
import DeleteProcess from './components/DeleteProcess.vue';
import SearchProcess from './components/SearchProcess.vue';

const routes = [
 { path: '/new', component: NewProcess},
 { path: '/view/:id', component:ViewProcess, name:'view'},
 { path: '/edit/:id', component:EditProcess, name:'edit'},
 { path: '/delete/:id', component:DeleteProcess, name:'delete'},
 { path: '/search', component:SearchProcess, name:'search'},
]

const router = new VueRouter({
 routes: routes
})

Vue.component('process-component', require('./components/ProcessComponent.vue'));

const app = new Vue({
    router
}).$mount('#app');
